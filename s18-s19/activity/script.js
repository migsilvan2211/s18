console.log("Hello from JS");

function Trainer(name, age, pokemon, Friends) {
	this.name = name;
	this.age = age;
	this.pokemon = pokemon;
	this.Friends = Friends;
}

let trainer = {
	name:"Miguel",
	age:"24",
	pokemon:["Pikachu", "Ho-Oh", "MewTwo"],
	Friends:  { names: ["Carlo", "Johannah", "Mariz", "Blazy", "Julina"] }
};

console.table(trainer);
console.log(trainer);